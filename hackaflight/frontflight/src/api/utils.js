import jwt from 'jwt-decode'
import axios from 'axios'

const ENDPOINT = 'http://localhost:7000'



export function login (user, password){
    try{
        axios.post(`${ENDPOINT}/auth`, {
            user: user,
            password: password
        })
        .then(function(response){
            console.log(response)
            // ME GUARDO EL TOKEN
            setAuthToken(response.data.token)
            // ME GUARDO EL ROL
            setIsAdmin(response.data.admin)
            // ME GUARDO EL NOMBRE DE USER
            setName(response.data.user)
        })
    }catch(error){
        console.log(error)
    }
}

export function register (name, user, email, password){
    try{
        axios.post(`${ENDPOINT}/registro`, {
            name: name,
            user: user,
            email: email,
            password: password
        })
        .then(function(response){
            console.log(response)
            // ME GUARDO EL TOKEN
            setAuthToken(response.data.token)
            // ME GUARDO EL USER DE USER
            setName(response.data.user)
        })
    }catch(error){
        console.log(error)
    }
}

// FUNCION PARA GUARDAR EN LOCALSTORAGE EL JSONWEBTOKEN
export function setAuthToken(token){
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    localStorage.setItem('AUTH_TOKEN_KEY', token)
}

// FUNCIÓN PARA RECUPERAR EL TOKEN DESDE EL LOCALSTORAGE
export function getAuthToken(){
    return localStorage.getItem('AUTH_TOKEN_KEY')
}

// FUNCION PARA CONSEGUIR LA FECHA DE CADUCIDAD DEL TOKEN
export function tokenExpiration(encodedToken){
    let token = jwt(encodedToken)
    if(!token.exp){
        return null
    }
    let date = new Date(0)
    date.setUTCSeconds(token.exp)
    return date
}

//FUNCION QUE COMPRUEBA SI EL TOKEN ESTÁ CADUCADO

export function isExpired(token){
    let expirationDate = tokenExpiration(token)
    return expirationDate < new Date()
}

// FUNCION QUE COMPRUEBA SI LA PERSONA ESTÁ LOGUEADA Y SU TOKEN ES VÁLIDO
export function isLoggedIn(){
    let authToken = getAuthToken()
    return !!authToken && !isExpired(authToken)
}

// FUNCION PARA GUARDAR ADMIN EN EL LOCALSTORAGE
export function setIsAdmin(admin){
    localStorage.setItem('ROLE', admin)
}

// FUNCION PARA RECUPERAR EL ADMIN DE LOCALSTORAGE
export function getIsAdmin(){
    return localStorage.getItem('ROLE')
}

// FUNCION PARA SABER SI ES ADMIN O NO
export function checkIsAdmin(){
    let role = null
    let admin = getIsAdmin()

    if(admin==='true'){
        role = true
    }else{
        role = false
    }
    return role
}
// FUNCION DE GUARDAR EL NOMBRE DEL USER EN EL LOCALSTORAGE
export function setName(user){
    localStorage.setItem('NAME', user)
}

// FUNCION DE RECUPERAR EL NOMBRE DEL USER EN LOCALSTORAGE
export function getName(){
    return localStorage.getItem('NAME')
}

// FUNCION DEL LOGOUT
export function logout(){
    axios.defaults.headers.common['Authorization'] = ''
    localStorage.removeItem('AUTH_TOKEN_KEY')
    localStorage.removeItem('ROLE')
    localStorage.removeItem('NAME')
}
