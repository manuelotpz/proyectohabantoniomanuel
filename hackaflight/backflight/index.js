
const emailjs = require('emailjs');
const express = require('express');
const { request, response } = require('express');
const app = express();
require("dotenv").config();

const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
//const { registerU, loginU } = require('./controllers/users');
const { isAuthenticated } = require("./middlewares/auth");
const { userExist, registerUser, loginUser } = require("./service/userService");
const { searchFlights } = require("./controllers/flightsAmadeus");
const port = process.env.PORT;

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());


//app.post('/users', registerU);

//app.post('/login', loginU);

app.get('/flights', searchFlights);


app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
  });

/*
localhost:8000/flights?originLocationCode=MAD&destinationLocationCode=LON&maxPrice=500&maxHours=2&maxMinutes=20&maxStops=0&departureDate=2020-08-31&adults=2
*/