// Declarando cosas que instalé
const config = require('./config')
const cors = require('cors')
const bodyParser = require('body-parser')
const mysql = require('mysql')
const express = require('express')
const app = express()
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
// Cosas que usa APP

app.use(cors())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

app.set('llave', config.llave)


// conexión a la BBDD
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'datoscliente'
})

// realizando conexión a BBDD
connection.connect(error => {
    if(error) throw error
    console.log('DATABASE CONECTADA :)')
})

// PUERTO DE CONEXIÓN DEL SERVICIO
const PORT = 7000

// CONEXIÓN DEL SERVICIO
app.listen(PORT, () => console.log('API CONECTADA :)'))


// RECOGER TODOS LOS CLIETES DE LA BASE
app.get('/', (req, res) => {
    res.send('Hola')
})

// AÑADIENDO CLIENTES A LA BASE DE DATOS - HACKAFLIGHT
app.post('/registro', (req, res) => {
    // SECUENCIA SQL
    const sql = 'INSERT INTO usuarios SET ?';
    // OBJETO DE DATOS DEL NUEVO CLIENTE
    const nuevoRegistro = {
        name: req.body.name,
        user: req.body.user,
        email: req.body.email,
        password: req.body.password
    }

    // CONEXION A LA BBDD
    connection.query(sql, nuevoRegistro, error => {
        if(error) throw error
        console.log('cliente creado con exito')
    })
    
})

// RECOGIENDO TODOS LOS CLIENTES DE LA BASE DE DATOS
app.get('/clientes', (req, res) => {
    // secuencia sql
    const sql='SELECT * FROM listaclientes'

    // conexion a BBDD
    connection.query(sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        }
        else{
            // si no hay clientes mostramos este dato
            console.log('No hay clientes que mostrar. :(')
        }
    })
})

// RECOGIENDO TODOS LOS PRODUCTOS DE LA BASE DE DATOS
app.get('/productos', (req, res) => {
    // secuencia sql
    const sql='SELECT * FROM listaproductos'

    // conexion a BBDD
    connection.query(sql, (error, results) => {
        if(error) throw error
        if(results.length > 0) {
            res.json(results)
        }
        else{
            // si no hay clientes mostramos este dato
            console.log('No hay productos que mostrar. :(')
        }
    })
})

// AÑADIR CLIENTES A LA BBDD
app.post('/add',(req, res) => {

    // SECUENCIA SQL
    const sql = "INSERT INTO listaclientes SET ?"

    // OBJETO DE DATOS DEL NUEVO CLIENTE
    const nuevoCliente = {
        nombre: req.body.nombre,
        usuario: req.body.usuario,
        contrasena: req.body.contrasena,
        email: req.body.email
    }

    // CONEXIÓN A BBDD
    connection.query( sql, nuevoCliente, error => {
        if(error) throw error
        console.log('Cliente creado con éxito :D')
    })

})

/*
// ACTUALIZANDO CLIENTES EN LA BBDD
app.put('/update/:id', (req, res) => {

    //DATOS QUE RECIBIMOS DE LA VISTA
    const id = req.params.id
    const nombre = req.body.nombre
    const usuario = req.body.usuario
    const contrasena = req.body.contrasena
    const email = req.body.email

    //SECUENCIA SQL
    const sql = `UPDATE listaclientes SET nombre='${nombre}', usuario='${usuario}', contrasena='${contrasena}', email='${email}' WHERE id=${id}`
    
    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Cliente actualizado con éxito :D')
    })
})

// BORRANDO CLIENTES EN LA BBDD
app.delete('/delete/:id', (req, res) => {
     
    //DATOS QUE RECIBIMOS DE LA VISTA
    const id = req.params.id

    //SECUENCIA SQL
    const sql = `DELETE FROM listaclientes WHERE id=${id}`
    
    //CONEXION A LA BBDD
    connection.query( sql, error => {
        if(error) throw error
        console.log('Cliente borrado con éxito :(')
    })

})
*/

// AUTH DE LA API

app.post('/auth', (req, res) => {

    // DATOS QUE RECIBO
    const user = req.body.user
    const password = req.body.password

    // SECUENCIA SQL
    const sql = `SELECT * FROM usuarios WHERE user='${user}' AND password='${password}'`
    
    // CONEXION A LA BBDD
    connection.query(sql, (error, results) =>{
        if(error) throw error
        if(results.length >0){
            console.log('Autenticacion exitosa')
            const payload = {
                check:true
            }
            // GUARDANDO NOMBRE DE USER
            let user = ''
            user = results [0].user

            // TOKEN
            const token = jwt.sign(payload, app.get('llave'),{
                expiresIn: '2 days'
            })
            res.json({
                mensaje: 'Autenticación completada con éxito',
                token: token,
                user: user
            })
        }
        else{
            console.log('Datos incorrectos.')
        }
    })
})
