import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import {isLoggedIn} from '../api/utils.js'


Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search.vue'),
    meta: {
      allowAnon: false
    }
  },
  /*
  {
    path: '/search',
    name: 'Search',
    component: () => import('../views/Search.vue'),
    meta: {
      allowAnon: false
    },
    beforeEnter: (to, from, next) => {
      if(to.meta.allowAnon=== false){
        next({
          path: '/',
          query:{redirect: to.fullPath}
        })
      }else{
        next()
      }
    }
  },
  */
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue'),
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/registrar',
    name: 'Registrar',
    component: () => import( '../views/Registrar.vue'),
    meta: {
      allowAnon: true
    }
  },
  {
    path: '/anonSearch',
    name: 'AnonSearch',
    component: () => import('../views/AnonSearch.vue'),
    meta: {
      allowAnon: true
    }
  },
  {
    path: '*',
    name: 'Error',
    component: () => import('../views/Error.vue'),
    meta: {
      allowAnon: true
    }
  }

]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if(!to.meta.allowAnon && !isLoggedIn()){
    next ({
      path: '/',
      query: {redirect: to.fullPath}
    })
  }else{
    next()
  }
})

export default router