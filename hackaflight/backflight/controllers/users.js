const express = require('express');
const { request, response } = require('express');
const app = express();

const userService = require('../service/userService');

const router = express.Router();

// Registro de usuarios
/* router.post('/users', async function(request, response) {
    const email = request.body.email;
    const password = request.body.password;

    if (await userService.userExist(email)) {
        const responseDTO = {'code': 200,
                            'description': 'El usuario (email) ya existe'};
        response.status(responseDTO.code).json(responseDTO);
        return;
    }

    const responseDTO = await userService.registerUser(email, password);
    response.status(responseDTO.code).json(responseDTO);
}); */

// Login de usuarios
/* router.post('/login', async function(request, response) {
    const email = request.body.email;
    const password = request.body.password;

    const responseDTO = await userService.loginUser(email, password)
    response.status(responseDTO.code).json(responseDTO);
}); */

// Registro de usuarios 2
const registerU = async (request, response) => {
    const email = request.body.email;
    const password = request.body.password;

    console.log(email);
    console.log(password);

    if (await userService.userExist(email)) {
        const responseDTO = {'code': 200,
                            'description': 'El usuario (email) ya existe'};
        response.status(responseDTO.code).json(responseDTO);
        return;
    }

    const responseDTO = await userService.registerUser(email, password);
    response.status(responseDTO.code).json(responseDTO);
};

// Login de usuarios 2
const loginU = async (request, response) => {
    const email = request.body.email;
    const password = request.body.password;

    const responseDTO = await userService.loginUser(email, password)
    response.status(responseDTO.code).json(responseDTO);
};

module.exports = {
    registerU,
    loginU
  };

//module.exports = router;
