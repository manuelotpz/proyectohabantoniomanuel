var http = require('http');
var fs = require('fs');

var index = require('airportsjs')
var Amadeus = require('amadeus');
const express = require('express');
const cors = require('cors');
const { request, response } = require('express');
const app = express();

var amadeus = new Amadeus({
  clientId: "mUacsaxX1J7vgVCckTV4BJtM0xZZ3R01",
  clientSecret: "AvxFRQ36L9hgUe9z"
});
app.use(cors())

const airlines = require('iata-airlines');
console.log(index.lookupByIataCode('EWR'));
console.log(index.searchByAirportName('Newark'));

const searchFlights = async (req, res) => {
  if (req.query.originLocationCode === undefined || req.query.originLocationCode.length === 0 ||
    req.query.destinationLocationCode === undefined || req.query.destinationLocationCode.length === 0 ||
    req.query.departureDate === undefined || req.query.departureDate.length === 0 ||
    req.query.adults === undefined || req.query.adults.length === 0 ||
    req.query.maxPrice === undefined || req.query.maxPrice.length === 0 ||
    req.query.maxHours === undefined || req.query.maxHours.length === 0 ||
    req.query.maxMinutes === undefined || req.query.maxMinutes.length === 0 ||
    req.query.maxStops === undefined || req.query.maxStops.length === 0
  ) {
    res.status(400).send('Faltan uno o más campos');
    return;
  }

  try {
    const response = await amadeus.shopping.flightOffersSearch.get({
      originLocationCode: req.query.originLocationCode,
      destinationLocationCode: req.query.destinationLocationCode,
      departureDate: req.query.departureDate,
      adults: req.query.adults,
    })

    maxPrice = req.query.maxPrice;
    maxHours = req.query.maxHours;
    maxMinutes = req.query.maxMinutes
    maxStops = req.query.maxStops;
    let datos = response.data
    let flights = []
    for (i = 0; i < datos.length; i++) {
      price = datos[i].price.grandTotal;
      hourHM = (datos[i].itineraries[0].duration.slice(2));
      hour_position = (datos[i].itineraries[0].duration.slice(2)).indexOf('H')
      hour = hourHM.slice(0, hour_position)
      minute = hourHM.slice(hour_position + 1, hourHM.length - 1)
      hour = parseInt(hour)
      minute = parseInt(minute)
      stops = datos[i].itineraries[0].segments.length - 1;
//console.log(datos[i].itineraries[0])
      if (price <= maxPrice && hour <= maxHours && minute <= maxMinutes && stops <= maxStops) {
        flights.push({
          // origen: datos[i].itineraries.originLocationCode,
          id: datos[i].id,
          //departure: datos[i].itineraries[0].segments[0].departure.iataCode + '>>' + datos[i].itineraries[0].segments[1].departure.iataCode,
          //arrival: datos[i].itineraries[0].segments[0].arrival.iataCode + '>>' + datos[i].itineraries[0].segments[1].arrival.iataCode,          
          AirlineCode: airlines.findWhere({ iata: datos[i].validatingAirlineCodes[0] }).get('name'),
          price: datos[i].price.grandTotal + ' ' + datos[i].price.currency,
          duration: (datos[i].itineraries[0].duration).split('PT').pop(','),
          //numberOfBookableSeats: datos[i].numberOfBookableSeats,
          stops: (datos[i].itineraries[0].segments.length) - 1
        })
      }
    }
    res.json(flights)
  } catch (responseError) {
    console.log('Error ', responseError.code);
  };
}
  
module.exports = {
    searchFlights,
}